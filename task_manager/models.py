from django.db import models
from core.models import TimeStampedModel, OrderedModel
from jsonfield import JSONField

from core.utils import PathAndRename

class Contract(models.Model):
	name = models.CharField(max_length=300)
	def __str__(self):
		return self.name


class Year(models.Model):
	year = models.CharField(max_length=4, verbose_name='Year')
	def __str__(self):
		return self.year

class Keyword(models.Model):
	name = models.CharField(max_length=300, verbose_name="Keyword")
	def __str__(self):
		return self.name

class Task(TimeStampedModel, OrderedModel):
	STATUS_CHOICES = (
    ('opn', 'Open'),
    ('lck', 'Locked'),
    ('fin', 'Finalized'),
    ('acc', 'Accepted'),
	)
	TYPE_CHOICES = (
    ('mjc', 'MJC'),
    ('ntw', 'Network'),
    ('prt', 'Partner'),
    ('inf', 'Information'),
	)
	task_year = models.ForeignKey(Year, verbose_name='Year', null=True, blank=True)
	head_task = models.ForeignKey('self', null=True, blank=True, verbose_name='The task above this', related_name='related_task')
	keywords = models.ManyToManyField(Keyword, blank=True, verbose_name='Keywords')
	status = models.CharField(max_length=3, choices=STATUS_CHOICES, default='opn', verbose_name='Status')
	task_type = models.CharField(max_length=3, choices=TYPE_CHOICES, default='mjc', verbose_name='Type')
	title = models.TextField(default='', verbose_name='Title')
	description = models.TextField(default='', blank=True, verbose_name='Description')
	additional_activity = models.ManyToManyField('self', blank=True, default=None)
	is_template = models.BooleanField(default=False, verbose_name='Is this report is template?')

	def __str__(self):
		return self.title

	def get_task_type(self):
		type_name = ''
		for i in self.TYPE_CHOICES:
			if i[0] == self.task_type:
				type_name = i[1]
		return type_name

	def get_task_status(self):
		task_status = ''
		for i in self.STATUS_CHOICES:
			if i[0] == self.status:
				task_status = i[1]
		return task_status

class TaskDetail(TimeStampedModel, OrderedModel):
	task = models.ForeignKey(Task, verbose_name='Task', related_name='details')
	description = models.TextField(blank=True, verbose_name='Description')
	number = models.DecimalField(max_digits=8, decimal_places=4, null=True, blank=True)
	task_date = models.DateField(auto_now=False, verbose_name='Date', blank=True, null=True)
	text = models.CharField(max_length=300, verbose_name='Text', default='', blank=True)
	long_text = models.TextField(verbose_name='Long text', default='', blank=True)
	attachment = models.FileField(
		upload_to=PathAndRename('attachments'), 
		verbose_name='Attachment', 
		default=None, 
		null=True, 
		blank=True)
	attachment_name = models.CharField(max_length=300, default="No file yet")
	def __str__(self):
		if self.attachment:
			return self.task.title + ", " + self.attachment_name
		else:
			return self.task.title