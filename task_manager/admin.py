from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from .models import *

@admin.register(Year)
class YearAdmin(ModelAdmin):
	pass

@admin.register(Contract)
class ContractAdmin(ModelAdmin):
	pass

@admin.register(Task)
class TaskAdmin(ModelAdmin):
	pass

@admin.register(TaskDetail)
class TaskDetailAdmin(ModelAdmin):
	pass

@admin.register(Keyword)
class KeywordAdmin(ModelAdmin):
	pass