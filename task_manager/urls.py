# -*- coding: utf-8 -*-



from django.conf.urls import patterns, include, url

from django.conf import settings

from .views import *


urlpatterns = patterns(
    '',
    url(r'^$', MainReportsView.as_view(), name='reports_main'),
    url(r'^(?P<pk>[\w-]+)/$', ReportsView.as_view(), name='reports'),
    url(r'^post-changes$', TakeChanges.as_view(), name='changes'),
)

