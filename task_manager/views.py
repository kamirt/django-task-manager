from django.shortcuts import render
from django.views.generic.base import TemplateView, View
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.http import JsonResponse
from django.utils import timezone
from django.conf import settings
from django.core import serializers

import datetime
import json
import os
from pytz import utc

from core.utils import is_mobile
from .models import *
from main_forms.mixins import AjaxableResponseMixin


class MainReportsView(TemplateView):
    template_name = 'desktop/reports-main.html'

    def get_template_names(self):
        if is_mobile(self.request):
            return ['desktop/reports.html']
        return super(MainReportsView, self).get_template_names()

    def get_context_data(self, **kwargs):
        context = super(MainReportsView, self).get_context_data(**kwargs)
        context['page_id'] = 'reports-main'
        context['years'] = Year.objects.all()
        context['main_tasks'] = Task.objects.filter(head_task=None)
        context['task_types'] = ['Information', 'MJC', 'Partner', 'Network']
        return context

class ReportsView(TemplateView):
    template_name = 'desktop/reports.html'


    def get_template_names(self):
        if is_mobile(self.request):
            return ['desktop/reports.html']
        return super(ReportsView, self).get_template_names()

    def get_context_data(self, **kwargs):
        head_task_id = int(kwargs['pk'])

        head_task = Task.objects.get(pk=head_task_id)


        context = super(ReportsView, self).get_context_data(**kwargs)
        context['page_id'] = 'reports'
        context['tasks'] = Task.objects.filter(head_task=head_task).order_by('order')
        context['head_task'] = head_task

        return context

class TakeChanges(AjaxableResponseMixin, View):
    def get(self, request):
        data = self.request.GET
        target = data.get('target', None)
        if not target:
            try:
                data = json.loads(list(data.keys())[0])
                target = data.get("target", None)
                print(target)
            except:
                pass
        if request.is_ajax():
            if target == 'reports-request':
                reports = Task.objects.exclude(related_task=None)
                data = serializers.serialize("json", reports, fields=['title'])
        return JsonResponse({'reports':data})

    def post(self, *args, **kwargs):
        data = self.request.POST
        files = self.request.FILES or None
        target = data.get('target', None)
        if not target:
            try:
                data = json.loads(list(data.keys())[0])
                target = data.get("target", None)
            except:
                pass


        if files:
            for f in files:
                attachment_file = files.get(f)
                name = str(files[f])
            attachment = TaskDetail.objects.get(pk=int(f))
            attachment.attachment = attachment_file
            attachment.attachment_name = name
            attachment.save()


        if target=="new_report":
            level = int(data['level'])+1
            head_task = Task.objects.get(pk=int(data['head']))
            task_to_order = Task.objects.filter(head_task=head_task).last()
            if task_to_order:
                order = task_to_order.order+1
            else:
                order = 1

            task = Task.objects.create(
                order=order,
                title=data['title'],
                description=data['description'],
                head_task=head_task,
                task_year=head_task.task_year
                )

            context = {
                'new_report': render_to_string('desktop/task.html', locals()),
            }

            return self.render_to_json_response(context)
        elif target=="changes":

            task = Task.objects.get(pk=int(data["report"]))
            changes = data["changes"]

            for item in changes:
                # TYPE OF CHANGE DIVISION
                if item['type'] == "detail":

                    description = item.get("description", None)
                    value = item.get("value", None)
                    if description or value:
                        detail = TaskDetail.objects.get(pk=int(item["pk"]))
                        if description:
                            detail.description = description
                        if value:
                            # DETAILS TYPE DIVISION
                            detype = item["detail_type"] 
                            if detype == "date":

                                detail.task_date = datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%S.%fZ')
                            elif detype == "number":
                                detail.number = value
                            elif detype == "text":
                                detail.text = value
                            elif detype == "long_text":
                                detail.long_text = value
                            elif detype == "attachment":
                                detail.attachment = value
                        detail.save()
                if item['type'] == 'headers':
                    title = item.get('title', None)
                    description = item.get('description', None)
                    if title:
                        task.title = title
                    if description:
                        task.description = description
                    task.save()

            context = {
                'details': render_to_string('desktop/details.html', locals()),
            }

            return self.render_to_json_response(context)

        elif target=="create_field":
            task = Task.objects.get(pk=int(data['task']))
            if data["type"]=="date":
                detail = TaskDetail.objects.create(task=task, task_date=timezone.now(), description="New date")
                context = {
                    'details': render_to_string('desktop/fields/date.html', locals()),
                }
                return self.render_to_json_response(context)
            elif data["type"]=="number":
                detail = TaskDetail.objects.create(task=task, number=1, description="New number")
                context = {
                    'details': render_to_string('desktop/fields/number.html', locals()),
                }
                return self.render_to_json_response(context)
            elif data["type"]=="text":
                detail = TaskDetail.objects.create(task=task, text="New text", description="New text description")
                context = {
                    'details': render_to_string('desktop/fields/text.html', locals()),
                }
                return self.render_to_json_response(context)
            elif data["type"]=="long_text":
                detail = TaskDetail.objects.create(task=task, long_text="New long text", description="New long text description")
                context = {
                    'details': render_to_string('desktop/fields/long_text.html', locals()),
                }
                return self.render_to_json_response(context)
            elif data["type"]=="attachment":
                detail = TaskDetail.objects.create(
                    task=task, 
                    attachment="No_file_yet",
                    description="New attachment")
                context = {
                    'details': render_to_string('desktop/fields/attachment.html', locals()),
                }
                return self.render_to_json_response(context)
        elif target=="remove_detail":
            detail = TaskDetail.objects.get(pk=int(data["detail"]))
            detail.delete()
            return HttpResponse(status=200)
        elif target=="remove_task":
            task = Task.objects.get(pk=int(data["task"]))
            task.delete()
            return HttpResponse(status=200)
        elif target=="delete_keyword":
            task = Task.objects.get(pk=int(data["task"]))
            keyword = Keyword.objects.get(pk=data["pk"])
            task.keywords.remove(keyword)
            task.save()
            return HttpResponse(status=200)
        elif target=="new_keyword":
            task = Task.objects.get(pk=int(data["task"]))
            try:
                keyword = Keyword.objects.get(name=data["value"])
                if task.keywords.filter(pk=keyword.pk).count():
                    return JsonResponse({'error':'Keyword ' + keyword.name + ' already exists in this task'})
                else:
                    task.keywords.add(keyword)
                    return JsonResponse({'id':keyword.pk})
            except Keyword.DoesNotExist:
                keyword = Keyword.objects.create(name=data["value"])
                task.keywords.add(keyword)
                return JsonResponse({'id':keyword.pk})
            #task.keywords.remove(keyword)
            task.save()
            return HttpResponse(status=200)
        elif target=="additional-activity":
            task = Task.objects.get(pk=int(data["task"]))
            activities = [];
            for report in data['activity_list']:
                activity = Task.objects.get(pk=report)
                if not task.additional_activity.filter(pk=report).count():
                    task.additional_activity.add(activity)
                    activities.append({'name': activity.title, 'pk': activity.pk})
                else:
                    return JsonResponse({'error':'Activity ' + activity.title + ' already exists in this task'})
            task.save()
            return JsonResponse({'activities': activities})

        elif target=="remove_activity":
            task = Task.objects.get(pk=int(data["task"]))
            activity = Task.objects.get(pk=int(data["activity"]))
            if task.additional_activity.filter(pk=int(data["activity"])).count():
                task.additional_activity.remove(activity)
            task.save()
            return HttpResponse(status=200)
        elif target=="change-status":
            task = Task.objects.get(pk=int(data["task"]))
            task.status = data["status"]
            task.save()
            return JsonResponse({"status": task.get_task_status()})
        return HttpResponse(status=200)

def handle_uploaded_file(f):
    with open('/srv/media/data.txt', 'wb+') as dest:
        for chunk in f.chunks():
            dest.write(chunk)