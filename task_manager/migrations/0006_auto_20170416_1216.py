# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0005_auto_20170416_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskdetail',
            name='task_date',
            field=models.DateField(null=True, blank=True, verbose_name='Date'),
        ),
    ]
