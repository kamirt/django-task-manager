# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0002_auto_20170416_0814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='head_task',
            field=models.ForeignKey(related_name='related_task', verbose_name='The task above this', to='task_manager.Task', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='order',
            field=models.PositiveIntegerField(verbose_name='Order', default=1),
        ),
        migrations.AlterField(
            model_name='taskdetail',
            name='order',
            field=models.PositiveIntegerField(verbose_name='Order', default=1),
        ),
        migrations.AlterField(
            model_name='taskdetail',
            name='task_date',
            field=models.DateTimeField(verbose_name='Date'),
        ),
    ]
