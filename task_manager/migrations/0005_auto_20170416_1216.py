# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0004_auto_20170416_1208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskdetail',
            name='task',
            field=models.ForeignKey(related_name='details', to='task_manager.Task', verbose_name='Task'),
        ),
        migrations.AlterField(
            model_name='taskdetail',
            name='task_date',
            field=models.DateField(verbose_name='Date', blank=True),
        ),
    ]
