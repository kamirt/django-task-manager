# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0003_auto_20170416_1207'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskdetail',
            name='task_date',
            field=models.DateField(verbose_name='Date'),
        ),
    ]
