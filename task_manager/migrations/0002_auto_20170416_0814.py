# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Year',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('year', models.CharField(verbose_name='Year', max_length=4)),
            ],
        ),
        migrations.RemoveField(
            model_name='task',
            name='year',
        ),
        migrations.AlterField(
            model_name='task',
            name='title',
            field=models.TextField(verbose_name='Title', default=''),
        ),
        migrations.AlterField(
            model_name='taskdetail',
            name='description',
            field=models.TextField(verbose_name='Description', blank=True),
        ),
        migrations.AddField(
            model_name='task',
            name='task_year',
            field=models.ForeignKey(verbose_name='Year', null=True, to='task_manager.Year', blank=True),
        ),
    ]
