# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import core.utils


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
            ],
        ),
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('date_added', models.DateTimeField(verbose_name='Date added', db_index=True, auto_now_add=True)),
                ('date_updated', models.DateTimeField(verbose_name='Date updated', db_index=True, auto_now=True)),
                ('order', models.PositiveIntegerField(verbose_name='Order', default=0)),
                ('year', models.CharField(verbose_name='Year', blank=True, max_length=4)),
                ('status', models.CharField(choices=[('opn', 'Open'), ('lck', 'Locked'), ('fin', 'Finalized'), ('acc', 'Accepted')], verbose_name='Status', default='opn', max_length=3)),
                ('task_type', models.CharField(choices=[('mjc', 'MJC'), ('ntw', 'Network'), ('prt', 'Partner'), ('inf', 'Information')], verbose_name='Type', default='mjc', max_length=3)),
                ('title', models.CharField(verbose_name='Title', default='', max_length=300)),
                ('description', models.TextField(verbose_name='Description', blank=True, default='')),
                ('is_template', models.BooleanField(verbose_name='Is this report is template?', default=False)),
                ('additional_activity', models.ManyToManyField(default=None, blank=True, to='task_manager.Task', related_name='_task_additional_activity_+')),
                ('head_task', models.ForeignKey(blank=True, to='task_manager.Task', verbose_name='The task above this', null=True)),
                ('keywords', models.ManyToManyField(verbose_name='Keywords', blank=True, to='task_manager.Keyword', default=None)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TaskDetail',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('date_added', models.DateTimeField(verbose_name='Date added', db_index=True, auto_now_add=True)),
                ('date_updated', models.DateTimeField(verbose_name='Date updated', db_index=True, auto_now=True)),
                ('order', models.PositiveIntegerField(verbose_name='Order', default=0)),
                ('description', models.CharField(verbose_name='Description', blank=True, max_length=300)),
                ('number', models.DecimalField(max_digits=8, null=True, blank=True, decimal_places=4)),
                ('task_date', models.DateTimeField(verbose_name='Date', auto_now_add=True)),
                ('text', models.CharField(verbose_name='Text', blank=True, max_length=300, default='')),
                ('long_text', models.TextField(verbose_name='Long text', blank=True, default='')),
                ('attachment', models.FileField(upload_to=core.utils.PathAndRename('attachments'), blank=True, default=None, verbose_name='Attachment', null=True)),
                ('task', models.ForeignKey(verbose_name='Task', to='task_manager.Task')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
