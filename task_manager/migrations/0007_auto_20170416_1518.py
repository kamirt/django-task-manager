# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0006_auto_20170416_1216'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='keywords',
        ),
        migrations.AddField(
            model_name='task',
            name='keywords',
            field=models.CharField(verbose_name='Keywords', blank=True, max_length=300),
        ),
        migrations.DeleteModel(
            name='Keyword',
        ),
    ]
