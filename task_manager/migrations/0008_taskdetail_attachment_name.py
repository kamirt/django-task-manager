# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0007_auto_20170416_1518'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskdetail',
            name='attachment_name',
            field=models.CharField(default='No file yet', max_length=300),
        ),
    ]
