# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0008_taskdetail_attachment_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(verbose_name='Keyword', max_length=300)),
            ],
        ),
        migrations.RemoveField(
            model_name='task',
            name='keywords',
        ),
        migrations.AddField(
            model_name='task',
            name='keywords',
            field=models.ManyToManyField(blank=True, null=True, to='task_manager.Keyword', verbose_name='Keywords'),
        ),
    ]
