// ACTIONS


	moment().locale('en')


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}






	function rollUpOrExpand(element){
		var target = element.parent().find('.element-content').first();
		if(!element.hasClass('fixed')){
			if(target.is(':visible')){
				target.slideUp('fast');
			} else {
				target.show('fast');
			}
		}
	}

	function showWarning(message, continueFunction){
		var alertBlock = $('#warning');
		alertBlock.find('p').html(message);
		alertBlock.show();
		$('#warning-ok').click(function(){
			alertBlock.hide();
			$('#shadow').fadeOut('fast');
			continueFunction();
		})
		$('#warning-cancel').click(function(){
			alertBlock.hide();
			$('#shadow').fadeOut('fast');
		})
		$('#shadow').fadeIn('fast');
	}



	$('.collapser').click(function(){
		rollUpOrExpand($(this))
	})

	var taskH;
	var taskid;
	var containerToEdit;
	var editMenu;
	var reportCreationForm = $('#new-report');
	var shadow = $('#shadow')
	var dataToPost = {};
	var listToPost = [];
	var usualTimeout = true;
	var taskOrder,
		taskTitle,
		taskDescription;
	var statusChoices = $('.status-container').first();
	var chooseActivityContainer = $('.reports-container-list').first();



	// HIDING ELEMENTS ABOVE THE SHADOW
	function hideForms(){
		shadow.fadeOut('fast');
		if(reportCreationForm.is(':visible')){
			reportCreationForm.hide();
		} else if($('.add-menu').is(':visible')){
			$('.add-menu').hide();
		} else if (chooseActivityContainer.is(':visible')){
			chooseActivityContainer.hide();
		} else if(statusChoices.is(':visible')) {
			statusChoices.hide();
		}
	}
	shadow.click(function(){
		hideForms();
	})
	function inputError(element, text){
		if (usualTimeout){
			element.addClass('input-warning');
			var message = $('<span></span>');
			message.addClass('warning-message');
			message.addClass('small');
			message.html(text);
			element.append(message);
			usualTimeout = false;
			setTimeout(function(){
				message.remove();
				usualTimeout = true;
			}, 1000)
		}
		

		
	}
	function sendInfo(dataToSend, elementToLoad, successFunction, type){
		if (type!='GET'){
			type='POST'
		}
		var preloader = $("<span>preloader</span>");
		preloader.addClass('preloader');
		preloader.insertAfter(elementToLoad.firstChild);
		$.ajax({
			type: type,
			url: 'http://' + window.location.host + '/reports/post-changes',
			data: dataToSend,
			processData: false,
			success: function(data){
			    preloader.remove();
			    successFunction(data);
			},
	        beforeSend: function (xhr, settings) {
	            //if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
	                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	            //}
	        },
		})

	}
	

	function showTools(e){
		if(!$(e.target).hasClass('disabled')){
			taskid = $(e.target).data('taskid');
			taskH = $(e.target);

			var buttonSet = $('#edit-button-set');
			taskH.parent().append(buttonSet);

			buttonSet.attr('data-taskid', taskid)
			editMenu = buttonSet;
			$('.edit-button-set').removeClass('hidden');
			$('.edit-button-set').show('fast');	
		}
	}
		$('.collapser').on('mouseover', function(e){
			showTools(e);
		});

// SHOW ADD NEW FIELD DIALOG
	$('.add-field-button').click(function(){
					$('#shadow').fadeIn('fast');
					var addMenu = $('.add-menu');
					addMenu.css('display', 'block');
					addMenu.css('zIndex', '1000');
		})


	$('.add-menu').find('button').click(function(){
		$('#shadow').fadeOut('fast');
		if($(this).hasClass('attachment-btn')){
			taskH.find('.details-container').append
		} else if($(this).hasClass('number-btn')){

		} else if($(this).hasClass('date-btn')){
			
		} else if($(this).hasClass('text-btn')){
			
		} else if($(this).hasClass('long-text-btn')){
			
		}
	})



function strip(html) {
    return html.replace(/<.*?>/g, '');
}

function editor(){
	containerToEdit = taskH.parent().find("[id =" + taskid + "][class = details-container]");
	$('.collapser').addClass('disabled');

	containerToEdit.find('.for-info').hide();
	containerToEdit.find('.for-edit').show('fast');
	var additionalButtons = $('#additional-button-set');
	editMenu.append(additionalButtons);
	editMenu.css('left', '-110px');
	editMenu.addClass('active');
	additionalButtons.show();
	taskH.addClass("fixed");
	taskOrder = taskH.html().slice(0,1);
	taskTitle = taskH.html().slice(3);
	taskDescription = taskH.parent().find('.report-description').first().html();
	taskH.html(taskTitle);
	taskH.hide();
	taskH.parent().find('.add-activity').click(function(e){
		addActivity();
	});
	// CLOSING THE EDIT MENU
	$('.fa-ban').click(function(){
		additionalButtons.hide();
		editMenu.css('left', '-35px');
		containerToEdit.find('.for-edit').hide();
		containerToEdit.find('.for-info').show('fast');
		editMenu.removeClass('active');
		$('.collapser').removeClass('disabled');
		$('.fa-dropdown-edit').hide();
		taskH.removeClass("fixed");
		taskH.show();
	});

	editMenu.on('mouseover', function(){
		var dropdown = $('.fa-dropdown-edit');
		if (editMenu.hasClass('active')){
			editMenu.append(dropdown);
			dropdown.show();
		}
		if(taskH.data('level')===3){
			dropdown.find('.add-report-button').hide()
		} else {
			dropdown.find('.add-report-button').show()
		}
	});

// DATA COLLECTION INIT

	var edits = taskH.parent().find('.details-container').first().find('.for-edit');
	edits.each(function(){
		// DATE
		if($(this).hasClass('date')){
			var detail = {type: 'detail'}
			detail.pk = $(this).data('pk');
			detail.detail_type = 'date';
			var description = $(this).find('.date-description');
			description.on('input', function(){
				detail.description = description.val();
			}) 
			var datePicker = $(this).find('#datepicker' + $(this).data('pk'));
			datePicker.datepicker({
				onSelect: function(){
					detail.value = datePicker.datepicker( "getDate" )
				},
			});
			listToPost.push(detail);
		}
		// NUMBER
		if($(this).hasClass('number')){
			var detail = {type: 'detail'}
			detail.pk = $(this).data('pk');
			detail.detail_type = 'number';
			var description = $(this).find('.number-description');
			description.on('input', function(){
				detail.description = description.val();
			})
			var itemInput = $(this).find('.number-input').first();
			itemInput.on('input', function(){
				if (isNaN(itemInput.val())){
					inputError(itemInput.parent(), "Numbers only")
				} else {
					detail.value = itemInput.val();
				}
			});
			listToPost.push(detail);
		}
		// TEXT
		if($(this).hasClass('text')){
			var detail = {type: 'detail'}
			detail.pk = $(this).data('pk');
			detail.detail_type = 'text';
			var description = $(this).find('.text-description');
			description.on('input', function(){
				detail.description = description.val();
			});
			var itemInput = $(this).find('.text-input').first();
			itemInput.on('input', function(){
				detail.value = itemInput.val();
			});
			listToPost.push(detail);
		}
		// LONG-TEXT
		if($(this).hasClass('long-text')){
			var detail = {type: 'detail'}
			detail.pk = $(this).data('pk');
			detail.detail_type = 'long_text';
			var description = $(this).find('.long-text-description');
			description.on('input', function(){
				detail.description = description.val();
			});
			var itemInput = $(this).find('.long-text-input').first();
			itemInput.on('input propertychange', function(){
				detail.value = itemInput.val();
			});
			listToPost.push(detail);
		}
		// ATTACHMENT
		if($(this).hasClass('attachment')){
			console.log('attachment')
			var detail = {type: 'detail'}
			detail.pk = $(this).data('pk');
			detail.detail_type = 'attachment';
			var description = $(this).find('.attachment-description').first();
			description.on('input', function(){
				detail.description = description.val();
			});
			var itemInput = $(this).find('.attachment-input').first();
			console.log(itemInput)
			itemInput.on('change', function(){
				var formdata = new FormData();
				var file = itemInput[0].files[0];
				formdata.append(detail.pk, file, file.name);
				console.log(file)
				console.log(formdata)
				var request = new XMLHttpRequest();

				request.open('POST', 'http://' + window.location.host + '/reports/post-changes');
				request.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
				request.send(formdata);
			});
			listToPost.push(detail);
		}
	})


}



$('#edit-button-set').find('.fa-pencil').parent().click(function(){
	editor();
}) // end of edit function

// REMOVING FIELDS
function removingFields(el){
	if(el.hasClass('detail')){
		var detail = el.parent().data('pk');
		var data = {};
		data.target = "remove_detail"
		data.detail = detail
		var block = el.parent().parent()
		sendInfo(JSON.stringify(data), el.parent(), function(){
			block.fadeOut('fast', function(){block.remove()})
		})
		// KEYWORDS
	} else if(el.hasClass('key')) {
		var info = {target:'delete_keyword', pk: el.parent().data('id'), task:taskid}
		sendInfo(JSON.stringify(info), el.parent(), function(){
			var keyword = taskH.parent().find('.keywords').first().find('#keyword' + el.parent().data('id'));
			keyword.remove();
			el.parent().remove();
		})
		
	}
}
$('.fa-times').click(function(e){
	var element = $(this);
	removingFields(element)
})

// CREATING NEW FIELDS
function appendToDetails(container, data){

	var createdDetail = $(data.details);
	container.append(createdDetail);
	createdDetail.find('.for-edit').show();
	console.log(createdDetail.find('.fa-times'))
	createdDetail.find('.fa-times').click(function(){
			removingFields($(this))
		});
	editor();
	$('.add-menu').hide();
}
$('.ui-button').click(function(){
	var typeOfField = $(this).hasClass('number-btn') ? 'number' :
					  $(this).hasClass('date-btn') ? 'date' :
					  $(this).hasClass('text-btn') ? 'text' :
					  $(this).hasClass('long-text-btn') ? 'long_text' :
					  $(this).hasClass('attachment-btn') ? 'attachment' : null

	var container = taskH.parent().find('.details-container').first();
	if (typeOfField) {
		sendInfo(
			JSON.stringify({target:'create_field', type: typeOfField, task: taskid}),
			container,
			function(data){
				appendToDetails(container, data);

			}
		) 
	}
});



// ADDING NEW REPORT
	$('.add-report-button').click(function(){
		shadow.fadeIn('fast')
		reportCreationForm.show();
	});
	$('#create-report-btn').click(function(){
		var titleInput = reportCreationForm.find('#new-report-title');
		var descriptionInput = reportCreationForm.find('#new-report-description');
		var newReportTitle = titleInput.val();
		var newReportDescripiton = descriptionInput.val();
		var level = taskH.data('level');
		if(newReportTitle.length < 1){
			inputError(reportCreationForm, 'Title can not be empty');
		} else {
			dataToPost = {
				target: 'new_report',
				level: level,
				title: newReportTitle,
				description: newReportDescripiton.length > 1 ? newReportDescripiton : " ",
				head: taskid,
			};
			sendInfo(JSON.stringify(dataToPost), taskH.parent(), function(data){

				var content = $(data.new_report);
				var parentContainer = taskH.parent().find('ul.element-list').first();
				if (parentContainer.has('li').length){
					parentContainer.append(content)
				} else {
					parentContainer.append(content);
				}
				content.find('.collapser').first().on('mouseover', function(e){
					showTools(e);
				});
				hideForms();
			});
		}
		//
	})

// ADD KEYWORD
var keywordValue; 
function addKeyword(element){
	var keyInput = element.parent().find('input');
	if(keywordValue!=keyInput.val().toLowerCase()){
		sendInfo(JSON.stringify({target:'new_keyword', task:taskid, value:keyInput.val().toLowerCase()}),
		element.parent(), function(data){
			if(data.error){
				showWarning(data.error, function(){})
			} else {
				var keyword = $('<span data-id=' + data.id + ' class="keyword keywords"><i class="fa fa-times key" aria-hidden="true"></i>' + keyInput.val() + '</span>')
				keyword.insertBefore(keyInput);
				var cross = keyword.find('.fa-times');
				cross.click(function(){
					removingFields(cross)
					keywordValue = '';
				});
				taskH.parent().find('.keywords').first().append('<span id="keyword' + data.id +'" class="keyword">' + keyInput.val() + '</span>')
			}
			
		})
		keywordValue = keyInput.val().toLowerCase();
	} else {
		showWarning('This keyword has been added already.', function(){})
	}
	
}
$('.add-keyword').click(function(e){
	addKeyword($(this));
});

// ADDITIONAL ACTIVITIES
function removeActivity(activity){
	var activityId = activity.parent().attr('id')[activity.parent().attr('id').length-1]
	sendInfo(JSON.stringify({target: 'remove_activity', task: taskid, activity: activityId}), taskH, function(data){
		activity.parent().remove();
		taskH.parent().find('.for-info.activity').first().find('.additional-activity' + activityId).remove();
	});
}

$('.delete-activity').click(function(){
	removeActivity($(this));
});

var actList = [];


function sendActivitiesList(activitiesList){
	sendInfo(JSON.stringify({target: 'additional-activity', task: taskid, activity_list: activitiesList}), taskH, function(data){
		// ADDITIONAL ACTIVITY ADDING
		if(data.error){
			showWarning(data.error, function(){})
		} else {
			for (var i=0; i<data.activities.length; i++){
				taskH.parent().find('.activity').first().append('<p class="additional-activity">' + data.activities[i].name + '</p>');
				taskH.parent().find('.activity').first().next().append('<p id=activity-select' +
			 	data.activities[i].pk +
			 	' class="additional-activity-select"><span>' +
			  	data.activities[i].name +
			  	'</span> <i class="fa fa-times delete-activity" aria-hidden="true"></i></p>');
			}
			$('#shadow').fadeOut('fast');
		}
		chooseActivityContainer.hide();

		
		actList = activitiesList;
		taskH.parent().find('.delete-activity').click(function(e){
			removeActivity($(this));
		});
	});
}
function fillActivityList(){
	var activityList = [];
	chooseActivityContainer.find('.report-record').each(function(){
		if($(this).hasClass('highlighted')){
			activityList.push($(this).attr('id'))
		}
	});
	return activityList;
}
function chooseActivity(){
 	var okBtn = $('<button>Ok</button>');
	chooseActivityContainer.show();
	chooseActivityContainer.append(okBtn);
	$('#shadow').fadeIn('fast');
	okBtn.click(function(){
		activityList = fillActivityList();
		console.log('1 ', actList)
		console.log('2 ', activityList)
		if(activityList!=actList){
			sendActivitiesList(activityList);
		}	
	})

		//console.log('activityList = ', activityList)
		




}

function createContainer(data){
	var reports = JSON.parse(data.reports);
	
	var reportId;
	chooseActivityContainer.empty();
	for(var activity=0; activity<reports.length; activity++){
		reportId = reports[activity].pk;
		var record = $('<p id=' + reportId + ' class="report-record"></p>');
		
		record.html(reports[activity].fields.title)
		chooseActivityContainer.append(record);

	}
	chooseActivityContainer.find('.report-record').click(function(e){
		var el = $(this);
		if (el.hasClass('highlighted')){
			el.removeClass('highlighted');
		} else {
			el.addClass('highlighted');
		}
	});

}
function addActivity(){

	var getActivityList = sendInfo(JSON.stringify({target:'reports-request'}), taskH, function(data){
		createContainer(data);
	
		var continueSending = chooseActivity();

	}, 'GET')
	taskH.parent().find('.delete-activity').click(function(e){
		removeActivity($(this));
	});
}




// DELETING REPORT
$('.delete-button').click(function(e){
	var data = {};
	data.target = "remove_task"
	data.task = taskid
	var parent = $(this).parent();
	function removeTask(id){
		if(id===taskid){
				sendInfo(JSON.stringify(data), parent, function(){
				$('body').append(editMenu);
				$('#additional-button-set').hide();
				editMenu.css('left', '-35px');

				editMenu.removeClass('active');
				$('.collapser').removeClass('disabled');
				$('.fa-dropdown-edit').hide();
				taskH.parent().fadeOut('fast', function(){taskH.parent().remove()})
				$('.collapser').removeClass('disabled');
			})
		}
	}
	showWarning("Are you sure? This will remove this report and alll the reports below", function(){
		removeTask(taskid)
	})
})

var currentStatus;
var currentTask;
var currentStatusBox;
// STATUS_CHANGING
$('.status').click(function(e){
	statusChoices.show();
	shadow.fadeIn('fast');
	currentStatus = $(this).parent().parent().find('.status').first().html();
	currentTask = $(this).data('task');
	currentStatusBox = $(this);
});

$('#status-ok').click(function(){
	sendInfo(JSON.stringify({target: 'change-status', task: currentTask, status: $('#status-select').val()}), statusChoices, function(data){
		currentStatusBox.html(data.status);
		currentStatusBox.removeClass(currentStatus);
		currentStatusBox.addClass(data.status);
		shadow.fadeOut('fast');
		statusChoices.hide();
	});
});
// SAVE CHANGES
	$('.fa-floppy-o').click(function(e){
		var headers = {type: 'headers'};
		var newReportTitle = taskH.parent().find('.for-edit.report-title').first().find('input').val();
		var newReportDescripiton = taskH.parent().find('.for-edit.report-description').first().find('input').val();
		
		
		if(taskH.html()!=newReportTitle){
			headers.title = newReportTitle;
		}
		if(taskH.parent().find('.report-description').first().html()!=newReportDescripiton){
			headers.description = newReportDescripiton;
		}
		if (headers) {
			listToPost.push(headers);
		}
		sendInfo(
			JSON.stringify({target: 'changes', changes: listToPost, report: taskid}),
			taskH, 
			function(data){
				var detContainer = taskH.parent().find('.details-container').first();
				detContainer.fadeOut('fast');
				detContainer.html(data.details);
				detContainer.fadeIn('fast');
				taskH.html(taskOrder + '. ' + newReportTitle);
				taskH.removeClass("fixed");
				taskH.parent().find('.report-description').first().html(newReportDescripiton);
				taskH.show();
				dataToPost = {};
				listToPost = [];
				var addButton = $(data).find('.add-activity');
				addButton.click(function(){
					addActivity();
				});
				$('.delete-activity').click(function(){
					removeActivity($(this));
				});
				$('.add-keyword').click(function(e){
					addKeyword($(this));
				});
			})

	});


		$('.collapser').on('mouseleave', function(e){
			if(!$(e.target).hasClass('disabled')){
				var pointed = false;
				$('.set-button').mouseover(function(){
					pointed = true;
				});

				setTimeout(function(){
					if(!pointed){
						$('.edit-button-set').hide('fast', function(){
							$('.edit-button-set').addClass('hidden');
							taskH.find('#edit-button-set').remove();
						});
					}
				}, 300);	
			}	
		});





