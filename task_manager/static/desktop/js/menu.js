$().ready(function() {
	
	// Set up menu buttons
	var elementMenus = {
			'report': { 'Self': 'Report', 'Element': 'Objective',
			 	removeButtons: {
					// 'Show Self':true, 'Hide Self':true
				}},
			'objective': { 'Self': 'Objective', 'Elements': 'Strategies', 'Element': 'Strategy',
				removeButtons: {
					// 'Expand All':true, 'Collapse All': true
				}},
			'strategy': { 'Self': 'Strategy', 'Elements': 'Activities', 'Element': 'Activity',
				removeButtons: {
					// 'Expand All':true, 'Collapse All': true
				}},
			'activity': { 'Self': 'Activity', 
				removeButtons: {
					'Expand All':true, 'Collapse All': true,
					'Add Element':true, 'Edit All Elements':true, 'Save All Elements':true,
					'Collapse Elements':true, 'Expand Elements':true, 'Show Hidden Elements':true,
					'Hide Faded Elements':true
				}}
		},
		
		elementButtons = {
				
			view: {
				title: 'View',
				method: 'toggle',
				plugin: 'collapsible',
				icon: 'search',
				text: false,
				hideOnClick: true,
				dropDown: {
					// 'Collapse Elements': { method: 'collapse', icon: 'arrowthickstop-1-n', hideClass: 'collapsed' },
					// 'Expand Elements': { method: 'expand', icon: 'arrowthickstop-1-s', hideClass: 'expanded' },
					'Expand All': { method: 'expandAll', icon: 'bullet' },
					'Collapse All': { method: 'collapseAll', icon: 'bullet' },
					'Get Link to this Self': { method: 'linkTo', icon: 'link' },
					// 'Show Hidden Elements' : { method: 'showHiddenElements', icon: 'bullet' },
					// 'Hide Faded Elements' : { method: 'hideHiddenElements', icon: 'radio-on' },
					// 'Hide Self' : { method: 'hide', icon: 'radio-on', buttonClass: 'hide-button' },
					// 'Show Self' : { method: 'show', icon: 'bullet', buttonClass: 'show-button' }
				}
				
			},
			edit: {
				title: 'Edit',
				method: 'edit',
				plugin: 'reportElement',
				icon: 'pencil',
				text: false,
				hideOnClick: true,
				pinOnClick: {
					prependButtons: {
						'Save': { method: 'save', icon: 'disk', text: false },
						'Cancel': { method: 'cancel', icon: 'cancel', text: false }
					}
				},
				dropDown: {
					'Add Element': { method: 'add', icon: 'plus', buttonClass: 'add-child-button' },
					'Add Field': { method: 'addField', icon: 'plus', buttonClass: 'add-field-button', pinOnClick: true },
					// 'Duplicate Self': { method: 'duplicate', icon: 'copy' },
					// 'Edit All Elements': { method: 'editAll', icon: 'pencil', hideClass: 'editing-all' },
					// 'Save All Elements': { method: 'saveAll', icon: 'disk', hideClass: 'not-editing-all' },
					'Duplicate Self' : { method: 'duplicateElement', buttonClass: 'duplicate-button' },
					'Add Comment' : { method: 'addComment', buttonClass: 'add-comment-button' },
					// 'Lock Self' : { method: 'lockElement', buttonClass: 'lock-button' },
					// 'Unlock Self' : { method: 'unlockElement', buttonClass: 'unlock-button' },
					'Delete Self' : { method: 'deleteElement', icon: 'trash', buttonClass: 'delete-button' }
				}
			}
		
		},
		
		elementMenuOptions = {
			buttons: elementButtons,
			parent: '.report-element',
			plugin: 'reportElement',
			hideClass: 'editing'
		}
		;

	// Initialize Element Button Menus
	$.each(elementMenus, function(type, m) {
		var elMenuOpts = $.extend(true, {}, elementMenuOptions);

		if (type === 'activity') {
			elMenuOpts.buttons.edit.dropDown['Add Supported Activity'] = {
				buttonClass: 'add-supported-button',
				method: 'addSupported',
				pinOnClick: true
			}
		}

		$('#hidden-elements .element-menu').clone().floatingActions(
			$.extend(elMenuOpts, {
				type: type,
				bind: '.' + type + '.title.heading',
				position: 'left',
				subs: m
			})
		);
	});

	// Field menu
	$('.field-menu').floatingActions({
		bind: '.field .in-place.title',
		parent: '.field',
		type: 'field',
		position: 'left',
		buttons: {
			'Delete Field': { method: 'deleteField', icon: 'trash', text: false }
		},
		plugin: 'reportField'
	});	

	// Comment menu
	$('.comment-menu').floatingActions({
		bind: '.comment .title',
		parent: '.comment',
		type: 'comment',
		position: 'left',
		buttons: {
			'Delete Comment': { method: 'deleteComment', icon: 'trash', text: false },
			'comment-actions': {
				method: 'actions', icon: 'gear', text: false
			},
			'Edit Comment': { method: 'edit', icon: 'pencil', text: false,
				hideOnClick: true,
				pinOnClick: {
					prependButtons: {
						'Save': { method: 'save', icon: 'disk', text: false },
						'Cancel': { method: 'cancel', icon: 'cancel', text: false }
					}
				}
			 }
		},
		plugin: 'reportComment'
	});

	// Temporary Unsavory Hack
	
});