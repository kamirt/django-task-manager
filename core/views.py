# -*- coding: utf-8 -*-
from django.utils import translation
from django.utils.translation import check_for_language
from django.views.generic.base import TemplateView, RedirectView
from django.views.generic.detail import DetailView
import random
from django.conf import settings

from core.models import *
from main_forms.mixins import RejectStaffUserMixin


__all__ = ['HomeView']


class HomeView(RejectStaffUserMixin, TemplateView):
    template_name = 'home.html'