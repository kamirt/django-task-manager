# -*- coding: utf-8 -*-
from autoslug.fields import AutoSlugField
from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import signals as signals
from parler.models import TranslatableModel, TranslatedFields
from core.utils import str_trunc
from django.utils.translation import ugettext_lazy as _

__all__ = ['CoreUser']


def create_admin_user(app_config, **kwargs):
    if app_config.name != 'core':
        return None

    try:
        CoreUser.objects.get(username='admin')
    except CoreUser.DoesNotExist:
        print('Creating admin user: login: admin, password: 123')
        assert CoreUser.objects.create_superuser('admin', 'admin@localhost', '123')
    else:
        print('Admin user already exists')


signals.post_migrate.connect(create_admin_user)


# Abstract

class TimeStampedModel(models.Model):
    date_added = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Date added')
    date_updated = models.DateTimeField(auto_now=True, db_index=True, verbose_name='Date updated')

    class Meta(object):
        abstract = True


class OrderedModel(models.Model):
    order = models.PositiveIntegerField(default=1, verbose_name='Order')

    class Meta(object):
        abstract = True
        ordering = ['order']


# User

class CoreUser(AbstractUser):
    pass