# -*- coding: utf-8 -*-
from django.contrib.admin.options import ModelAdmin, StackedInline
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib import admin
from django import forms
import reversion

from core.models import *


class CoreUserChangeForm(UserChangeForm):
    class Meta:
        model = CoreUser
        fields = '__all__'


class CoreUserCreationForm(UserCreationForm):
    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            CoreUser._default_manager.get(username=username)
        except CoreUser.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )

    class Meta:
        model = CoreUser
        fields = '__all__'


class CoreUserAdmin(reversion.VersionAdmin, UserAdmin):
    form = CoreUserChangeForm
    add_form = CoreUserCreationForm
    change_list_template = 'smuggler/change_list.html'


class OrderedAdmin(ModelAdmin):
    # mandatory display & editable 'order' field
    list_display = ('order',)
    list_editable = ('order',)
    ordering = ('order',)

    class Media:
        js = ['/static/js/admin_list_reorder.js', ]


StackedInline.sortable_field_name = ''  # fix for grapelli sortable_field_name error

admin.site.register(CoreUser, CoreUserAdmin)