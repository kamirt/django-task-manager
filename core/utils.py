# -*- coding: utf-8 -*-
from ipware.ip import get_real_ip
import random
import string
import os
from django.utils.deconstruct import deconstructible
from devserver.modules import DevServerModule
from devserver.modules.sql import sqlparse

try:
    from django.db import connections
except ImportError:
    # Django version < 1.2
    from django.db import connection

    connections = {'default': connection}


def str_trunc(str, max_length=50):
    if len(str) > max_length:
        return u'{0}...'.format(str[:max_length])
    return str


def generate_uid():
    chars = string.digits + string.ascii_letters

    return ''.join(random.choice(chars) for _ in range(10))

@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # set filename as random string
        uid = generate_uid()
        filename = '{}.{}'.format(uid, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)

def get_user_uid(request):
    user_uid = get_real_ip(request)

    # uncomment to local work
    # if not user_uid:
    # user_uid = get_ip(request)

    if not user_uid:
        user_uid = request.session.get('user_uid', None)
        if not user_uid:
            user_uid = generate_uid()

            request.session['user_uid'] = user_uid
    return user_uid


class SQLRealTimeModuleEx(DevServerModule):
    """
    Outputs SQL queries as they happen.
    """

    logger_name = 'sql'

    def process_complete(self, request):
        queries = [
            q for alias in connections
            for q in connections[alias].queries
        ]
        for query in queries:
            self.logger.info(
                sqlparse.format(query['sql'], reindent=True, keyword_case='upper'),
                duration=float(query.get('time', 0)) * 1000
            )

MOBILE_USERAGENTS = ("2.0 MMP", "240x320", "400X240", "AvantGo", "BlackBerry",
                     "Blazer", "Cellphone", "Danger", "DoCoMo", "Elaine/3.0", "EudoraWeb",
                     "Googlebot-Mobile", "hiptop", "IEMobile", "KYOCERA/WX310K", "LG/U990",
                     "MIDP-2.", "MMEF20", "MOT-V", "NetFront", "Newt", "Nintendo Wii", "Nitro",
                     "Nokia", "Opera Mini", "Opera Mobi", "Palm", "PlayStation Portable", "portalmmm", "Proxinet",
                     "ProxiNet", "SHARP-TQ-GX10", "SHG-i900", "Small", "SonyEricsson", "Symbian OS",
                     "SymbianOS", "TS21i-10", "UP.Browser", "UP.Link", "webOS", "Windows CE",
                     "WinWAP", "YahooSeeker/M1A1-R2D2", "iPhone", "iPod", "iPad", "Android",
                     "BlackBerry9530", "LG-TU915 Obigo", "LGE VX", "webOS", "Nokia5800")


def is_mobile(request):
    return any(ua for ua in MOBILE_USERAGENTS if ua in request.META.get("HTTP_USER_AGENT", ''))