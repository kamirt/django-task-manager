# -*- coding: utf-8 -*-


from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.utils.encoding import iri_to_uri
from django.conf import settings
from filebrowser.sites import site

from core.views import *
from task_manager import urls as tasks_urls

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^reports/', include(tasks_urls)),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^admin-grappelli/', include('grappelli.urls')),
    url(r'^admin/', include('smuggler.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    media_url = settings.MEDIA_URL[1:] if settings.MEDIA_URL.startswith('/') \
        else settings.MEDIA_URL

    urlpatterns += patterns(
        '',
        (r'^{0}(?P<path>.*)$'.format(iri_to_uri(media_url)),
         'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}
        ),
    )