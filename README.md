## Django Task Manager
 
To run this project you should have a virtual environment with Python3 inside.

 **1)** To setup this virtual environment, just type:
```sudo pip install virtualenv```

**2)** Then you should create a new environment with Python 3, like:
```virtualenv -p python3 envname```, where 'envname' is the name of your environment.

**3)** Change the directory:
```cd envname```
 
**4)** Clone this repo:
```git clone https://kamirt@bitbucket.org/kamirt/django-task-manager.git```

**5)** Change directory again:
```cd django-task-manager```

**6)** Setup from requirements:
```pip install -r requirements.txt```

**7)** Inside 'project' folder you'll see 'settings_local_example.py' file. Just rename it into 'settings_local.py'. Here we should set the database. Find DATABASES variable inside this file. If you're already using PostgreSQL, just type your data inside. You should change NAME, USER and PASSWORD. If you're not a postgres user, you can change whole this variable to next:
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'yourdatabasename.db',
    }
}
```
**8)** The last action is typing next:
```python manage.py migrate```
```python manage.py runserver```

Done! Now you can access the project by typing 'http://127.0.0.1:8000' in your browser.
Note that administration panel is accessing by: 'http://127.0.0.1:8000/admin'

**Default admin user is:**
login: 'admin',
password: '123'

**Project structure:**

**Back-end:**
Database structure is quite simple: Task model is a ForeignKey of itself and has another ForeignKeys: Keywords, Year and AdditionalActivities(self again).

The main routing controlled by "project" app, including urls from "core" and "task_manager", "core" for home and maybe another stuff in the future and "task_manager" for the main application.

Views. This app uses class-based views. We have just 4 of them: 
**1)** for the list of main tasks/reports **(task_manager.Views.MainReportsView)**;
**2)** for the list of subtask of one of the main reports **(task_manager.Views.ReportsView)**;
**3)** AJAX view for receiving changes **(task_manager.Views.TakeChanges)**. The type of change controlled by "target" property. For example: {"target": "new_report"} means that we want to create a new report. If we need to return some changes (for example, report's detail or list of details), we render theese changes into special html template before serializing and then return ready-to-paste DOM.

**Front-end:**

Frontend consists of two reliable parts: templates and javascript controlling functions.

Template structure of the report's list:
```
<ul>
	<li>
	 <h1>Report title</h1>
	 <div>Class details</div>
		 <ul>
		 	<li>
		 		<h2>Report title</h2>
		 		<div>Class details</div>
		 			<ul>etc...</ul>
		 	</li>
		 </ul>
	 </li>
 </ul>
 ```

So we have uncluded structure of the DOM and can control any depencies. 
Javascript code uses two main global variables: ```taskH``` and ```taskid```. When we click on "edit"(a pencil), this code writes this <h> tag into "taskH" variable and disable another <li> and <h> elements to be active. So until we click "save" or "cancel" our ```taskH``` and ```taskid``` variable let us work with one report, collect and paste information within one certain <li> tag.

Also, details of reports are dublicated within ".for-info" and ".for-edit" div's. ".for-info" shows the information, and ".for-edit" shows inputs to change this information. By clicking "edit", "save" or "cancel" button we can switch between this two states.

By the end, we have some service functions like:

- getCookie: to get csrf cookie value to protect POSTing from cross-domain requests;
- sendInfo: high-level abstraction above $.ajax. To get and post data with the same parameters;
- showWarning: standart "alert()"'s are not very beautiful and can be prevented, so this function shows nice alerts, for example, if we trying to add a keyword if this keyword is already in this report;
- rollOrExpand: for show\hide reports;
- hideForms: used when we click on #shadow div. This div is semi-transparent and black-coloring. This is one of the usual practices to hide or cancel something by clicking the element like this.
- editor and showTools functions: just do this fixing of <h> element, disallow us to edit more than one report per time and control edit buttons.

